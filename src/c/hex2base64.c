#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const char codes[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

char * b64_encode(char* bytes, int len)
{
  int n = (len * 4) / 3;
  char *out = malloc(sizeof(char) * n);
  int b;

  for (int i = 0; i < len; i += 3) {
    b = (bytes[i] & 0xFC) >> 2;
    out[4 * (i/3)] = codes[b];

    b = (bytes[i] & 0x3) << 4;
    if (i + 1 < len) {
      b |= (bytes[i+1] & 0xF0) >> 4;
      out[1 + 4 * (i/3)] = codes[b];

      b = (bytes[i+1] & 0xF) << 2;
      if (i + 2 < len) {
	b |= (bytes[i+2] & 0xC0) >> 6;
	out[2 + 4 * (i/3)] = codes[b];

	b = bytes[i+2] & 0x3F;
	out[3 + 4 * (i/3)] = codes[b];
      }
      else {
	out[2 + 4 * (i/3)] = codes[b];
	out[3 + 4 * (i/3)] = codes[64];
      }
    }
    else {
      out[1 + 4 * (i/3)] = codes[b];
      out[2 + 4 * (i/3)] = codes[64];
      out[3 + 4 * (i/3)] = codes[64];
    }
  }

  return out;
}

char * str2bytes(char in[]) {
  int n = strlen(in) / 2;
  char *out = malloc(sizeof(char) * n);

  for (int i = 0; i < n; i++) {
    int b = 0;
    for (int j = 0; j < 2; j++) {
      char c = in[2 * i + j];
      if (c - '0' < 10) {
	b |= (c - '0') << 4 * (1 - j);
      }
      else if (c - 'a' < 6) {
	b |= (10 + c - 'a') << 4 * (1 - j);
      }
      else if (c - 'A' < 6) {
	b |= (10 + c - 'A') << 4 * (1 - j);
      }
    }
    out[i] = b;
  }

  return out;
}

int main(int argc, char *argv[])
{
  char* s = argv[1];
  char* s_enc = b64_encode(str2bytes(s), strlen(s) / 2);
  printf("%s\n", s_enc);
  free(s_enc);
  return 0;
}
