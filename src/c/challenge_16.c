#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include "tob_rand.h"
#include "pkcs7.h"

#define MAX_LEN 0x1000
#define BLOCK_SIZE 0x10

unsigned char key[BLOCK_SIZE];


void handle_errors(void)
{
  ERR_print_errors_fp(stderr);
  abort();
}

int ecb_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
		unsigned char *iv, unsigned char *ciphertext)
{
  EVP_CIPHER_CTX *ctx;
  int len;
  int ciphertext_len;

  /* Initialise context */
  if (!(ctx = EVP_CIPHER_CTX_new()))
    handle_errors();

  if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, iv))
    handle_errors();

  if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
    handle_errors();
  ciphertext_len = len;

  EVP_CIPHER_CTX_free(ctx);

  return ciphertext_len;
}

int ecb_decrypt(unsigned char *ciphertext, int ciphertext_len,
		unsigned char *key, unsigned char *iv, unsigned char *plaintext)
{
  EVP_CIPHER_CTX *ctx;
  int len;
  int plaintext_len;

  /* Initialise context */
  if (!(ctx = EVP_CIPHER_CTX_new()))
    handle_errors();

  if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, iv))
    handle_errors();

  if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
    handle_errors();
  plaintext_len = len;

  EVP_CIPHER_CTX_free(ctx);

  return plaintext_len;
}

void xor_buffer(unsigned char *buf, unsigned char *vals, int vals_len)
{
  for (int i = 0; i < vals_len; i++) {
    buf[i] = buf[i] ^ vals[i];
  }
}

int cbc_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
		unsigned char *iv, unsigned char *ciphertext)
{
  int block_size = BLOCK_SIZE;
  int i = 0;
  int m = 0;
  char plaintext_block[block_size];
  char ciphertext_block[block_size];

  while (i < plaintext_len) {
    memcpy(plaintext_block, plaintext + i, block_size);
    xor_buffer(plaintext_block, iv, block_size);

    ecb_encrypt(plaintext_block, block_size, key, NULL, ciphertext_block);

    memcpy(ciphertext + i, ciphertext_block, block_size);
    memcpy(iv, ciphertext_block, block_size);

    i += block_size;
  }

  return i;
}

int cbc_decrypt(unsigned char *ciphertext, int ciphertext_len,
		unsigned char *key, unsigned char *iv,
		unsigned char *plaintext)
{
  int block_size = BLOCK_SIZE;
  int i = 0;
  int m = 0;
  char plaintext_block[block_size];
  char ciphertext_block[block_size];

  while (i < ciphertext_len) {
    memcpy(ciphertext_block, ciphertext + i, block_size);
    ecb_decrypt(ciphertext_block, block_size, key, NULL, plaintext_block);

    xor_buffer(plaintext_block, iv, block_size);

    memcpy(plaintext + i, plaintext_block, block_size);
    memcpy(iv, ciphertext_block, block_size);

    i += block_size;
  }

  return i;
}

int encrypt(char *key, char* iv, char *user_input, char *ciphertext) {
  char prefix[] = "comment1=cooking%20MCs;userdata=";
  char suffix[] = ";comment2=%20like%20a%20pound%20of%20bacon";

  int n_p = strlen(prefix);
  int n_u = strlen(user_input);
  int n_s = strlen(suffix);

  int n = 0;
  char plaintext[MAX_LEN] = {0};

  memcpy(plaintext, prefix, n_p);
  n += n_p;
  for (int i = 0; i < n_u; i++)
    if (user_input[i] != ';' && user_input[i] != '=') {
      plaintext[n++] = user_input[i];
    }
  memcpy(plaintext + n, suffix, n_s);
  n += n_s;

  char *padded_plaintext = pkcs7_pad(BLOCK_SIZE, plaintext);

  n = cbc_encrypt(padded_plaintext, strlen(padded_plaintext), key, iv, ciphertext);

  return n;
}


int fun_1(char *user_input, char *ciphertext) {
  unsigned char iv[BLOCK_SIZE] = { 0 };

  int n = encrypt(key, iv, user_input, ciphertext);

  return n;
}

int fun_2(char *ciphertext, int ciphertext_len) {
  unsigned char iv[BLOCK_SIZE] = { 0 };
  char plaintext[MAX_LEN] = { 0 };
  char *pch;
  int result;

  cbc_decrypt(ciphertext, ciphertext_len, key, iv, plaintext);

  pch = strstr(plaintext, ";admin=true;");

  result = pch ? 1 : 0;

  if (result == 1)
    BIO_dump_fp(stdout, plaintext, ciphertext_len);

  return result;
}

int main(int argc, char *argv[])
{
  /* Initialise the libarary */
  ERR_load_crypto_strings();
  OpenSSL_add_all_algorithms();
  OPENSSL_config(NULL);

  write_random_bytes(key, BLOCK_SIZE);

  char ciphertext[MAX_LEN] = { 0 };
  int n = fun_1("xxxxxxxxxxxxxxxxxadminxtrue me you're looking for", ciphertext);

  printf("Fiddling bits, please wait ...\n");
  int i, j, k;
  int r = 0;
  for (i = 0; i < 0x100; i++) {
    if (r == 1)
      break;
    for (j = 0; j < 0x100; j++) {
      if (r == 1)
	break;
      for (k = 0; k < 0x100; k++) {
	if (r == 1)
	  break;
	ciphertext[0x20] = i;
	ciphertext[0x20 + 6] = j;
	ciphertext[0x20 + 11] = k;

	r = fun_2(ciphertext, n);
      }
    }
  }

  /* Clean up */
  EVP_cleanup();
  ERR_free_strings();

  return 0;
}
