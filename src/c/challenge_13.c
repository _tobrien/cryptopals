#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include "base64.h"
#include "tob_rand.h"

#define MAX_LEN 0x100
#define BLOCK_SIZE 0x10

typedef struct {
  char *email;
  char *user_id;
  char *role;
} user;


void pprint_user(user *user) {
  printf("%s\t%s\t%s\n", user->user_id, user->email, user->role);
}

char *val_for (char kvs[], char pattern[]) {
  int i;
  char *v = malloc(sizeof(char) * MAX_LEN);
  for (i = 0; i < MAX_LEN; i++)
    v[i] = 0;

  char *a, *b;
  int len;

  a = strstr(kvs, pattern) + strlen(pattern);
  b = strstr(a, "&");
  len = b ? b - a : kvs + strlen(kvs) - a;

  i = 0;
  while (i < len && !isspace(a[i])) {
    v[i] = a[i];
    i++;
  }

  return v;
}

user *decode_profile (char kvs[]) {
  user *profile = malloc(sizeof(user));
  char *email = val_for(kvs, "email=");
  char *role = val_for(kvs, "role=");
  char *uid = val_for(kvs, "uid=");

  profile->email = email;
  profile->role = role;
  profile->user_id = uid;

  /* profile->role = val_for(kvs, "role="); */
  /* profile->user_id = atoi(val_for(kvs, "uid=")); */

  /* printf("%d\n", profile->user_id); */
  /* printf("%s\n", profile->role); */

  return profile;
}

char *encode_profile (user *profile) {
  char *profile_str = malloc(sizeof(char) * 3 * MAX_LEN);

  strcat(profile_str, "email=");
  strcat(profile_str, profile->email);
  strcat(profile_str, "&uid=");
  strcat(profile_str, profile->user_id);
  strcat(profile_str, "&role=");
  strcat(profile_str, profile->role);

  return profile_str;
}

char *profile_for(char *email) {
  char sanitised_email[MAX_LEN] = {0};
  char *c = email;
  int i = 0;
  while (*c != '\0') {
    if (!(*c == '&' || *c == '='))
      sanitised_email[i++] = *c;
    c++;
  }

  user profile;
  profile.email = sanitised_email;
  profile.user_id = "10";
  profile.role = "user";

  return encode_profile(&profile);
}

void handle_errors(void)
{
  ERR_print_errors_fp(stderr);
  abort();
}

char *pkcs7 (int block_size, char plaintext[]) {
  int n = sizeof(char) * strlen(plaintext);
  int q = n / block_size;
  int r = n % block_size;
  int p = block_size - r;

  char *padded_plaintext = malloc(n + p);
  memcpy(padded_plaintext, plaintext, n);

  for (int i = n; i < n + p; i++) {
    padded_plaintext[i] = p;
  }

  padded_plaintext[n + p] = '\0';

  return padded_plaintext;
}

int ecb_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
		unsigned char *iv, unsigned char *ciphertext)
{
  EVP_CIPHER_CTX *ctx;
  int len;
  int ciphertext_len;

  /* Initialise context */
  if (!(ctx = EVP_CIPHER_CTX_new()))
    handle_errors();

  if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, iv))
    handle_errors();

  if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
    handle_errors();
  ciphertext_len = len;

  /* if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) */
  /*   handle_errors(); */
  /* ciphertext_len += len; */

  EVP_CIPHER_CTX_free(ctx);

  return ciphertext_len;
}

int ecb_decrypt(unsigned char *ciphertext, int ciphertext_len,
		unsigned char *key, unsigned char *iv, unsigned char *plaintext)
{
  EVP_CIPHER_CTX *ctx;
  int len;
  int plaintext_len;

  /* Initialise context */
  if (!(ctx = EVP_CIPHER_CTX_new()))
    handle_errors();

  if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, iv))
    handle_errors();

  if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
    handle_errors();
  plaintext_len = len;

  /* if (1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len)) */
  /*   handle_errors(); */
  /* plaintext_len += len; */

  EVP_CIPHER_CTX_free(ctx);

  return plaintext_len;
}

char *encrypt_encoded_profile(unsigned char *key, unsigned char *encoded_profile,
			      unsigned int len) {
  unsigned int size = 0;
  while (size < len) {
    size += BLOCK_SIZE;
  }
  unsigned char *ciphertext = malloc(sizeof(char) * size);

  unsigned char *padded_profile = pkcs7(BLOCK_SIZE, encoded_profile);

  ecb_encrypt(padded_profile, size, key, NULL, ciphertext);

  free(padded_profile);

  return ciphertext;
}

char *decrypt_encoded_profile(unsigned char *key, unsigned char *encrypted_profile,
			      unsigned int len) {
  unsigned int size = 0;
  while (size < len) {
    size += BLOCK_SIZE;
  }
  unsigned char *encoded_profile = malloc(sizeof(char) * size);

  ecb_decrypt(encrypted_profile, size, key, NULL, encoded_profile);

  /* remove padding */
  for (int i = len; i < size; i++) {
    encoded_profile[i] = '\0';
  }


  return encoded_profile;
}

int main(int argc, char *argv[])
{
  /* Initialise the library */
  ERR_load_crypto_strings();
  OpenSSL_add_all_algorithms();
  OPENSSL_config(NULL);

  /* Set key and IV */
  unsigned char key[0x10];
  write_random_bytes(key, 0x10);

  char *encoded_profile;
  char *encrypted_profile;
  char *tampered_encoded_profile;
  user *tampered_user;
  unsigned int len;

  /* Generate profile with `admin   ...` in a single block */
  encoded_profile = profile_for("aaaaaaaaaaadmin           ");

  len = strlen(encoded_profile);
  encrypted_profile = encrypt_encoded_profile(key, encoded_profile, len);

  /* Pick out admin block */
  char admin_block[BLOCK_SIZE];
  memcpy(admin_block, encrypted_profile + BLOCK_SIZE, BLOCK_SIZE);

  
  /* Generate new profile and overwrite block with encrypted admin block */
  encoded_profile = profile_for("a@xxxxxxx.com");
  len = strlen(encoded_profile);
  encrypted_profile = encrypt_encoded_profile(key, encoded_profile, len);
  memcpy(encrypted_profile + 2 * BLOCK_SIZE, admin_block, BLOCK_SIZE);


  /* Decrypt user */
  tampered_encoded_profile = decrypt_encoded_profile(key, encrypted_profile, 3 * BLOCK_SIZE);
  tampered_user = decode_profile(tampered_encoded_profile);


  pprint_user(tampered_user);

  /* Clean up */
  EVP_cleanup();
  ERR_free_strings();

  return 0;
}
