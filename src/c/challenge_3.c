#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "xor.h"
#include "utils.h"

const char characters[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

bool is_printable(char c)
{
  return 0x20 <= c && c < 0x7f;
}

bool is_vowel(char c) {
  if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' ||
      c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U') {
    return true;
  } else {
    return false;
  }
}

bool is_alpha(char c) {
  return ('A' == ' ' || 'A' <= c && c <= 'Z' || 'a' <= c && c <= 'z');
}

int main(int argc, char *argv[])
{
  char *a = argv[1];
  char *a_enc = str2bytes(a);

  char b[strlen(a) / 2];

  for (int i = 0; i < strlen(characters); i++) {
    for (int j = 0; j < strlen(a) / 2; j++) {
      b[j] = characters[i];
    }
    b[strlen(a) / 2] = '\0';

    char *out = fixed_xor(a_enc, b, strlen(a) / 2);
    int character_frequency = 0;
    int vowel_frequency = 0;
    int printable_frequency = 0;
    for (int k = 0; k < strlen(a) / 2; k++) {
      if (is_vowel(out[k]))
	vowel_frequency += 1;
      if (is_alpha(out[k]))
	character_frequency += 1;
      if (is_printable(out[k])) {
	printable_frequency += 1;
	printf("%c", out[k]);
      } else {
	printf(" ");
      }
    }

    printf("\t%c\t%d\n", b[0], character_frequency + vowel_frequency + printable_frequency);

    free(out);
  }

  free(a_enc);
  
  return 0;
}
