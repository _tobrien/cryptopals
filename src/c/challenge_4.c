#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include "xor.h"
#include "utils.h"

bool is_printable(char c)
{
  return 0x20 <= c && c < 0x7f;
}

float score(char * c, int n) {
  float score = 0.0;
  for (int i = 0; i < n; i++) {
    switch (tolower(c[i]))
      {
      case 'a':
	score += 0.0651738;
      case 'b':
	score += 0.0124248;
      case 'c':
	score += 0.0217339;
      case 'd':
	score += 0.0349835;
      case 'e':
	score += 0.1041442;
      case 'f':
	score += 0.0197881;
      case 'g':
	score += 0.0158610;
      case 'h':
	score += 0.0492888;
      case 'i':
	score += 0.0558094;
      case 'j':
	score += 0.0009033;
      case 'k':
	score += 0.0050529;
      case 'l':
	score += 0.0331490;
      case 'm':
	score += 0.0202124;
      case 'n':
	score += 0.0564513;
      case 'o':
	score += 0.0596302;
      case 'p':
	score += 0.0137645;
      case 'q':
	score += 0.0008606;
      case 'r':
	score += 0.0497563;
      case 's':
	score += 0.0515760;
      case 't':
	score += 0.0729357;
      case 'u':
	score += 0.0225134;
      case 'v':
	score += 0.0082903;
      case 'w':
	score += 0.0171272;
      case 'x':
	score += 0.0013692;
      case 'y':
	score += 0.0145984;
      case 'z':
	score += 0.0007836;
      case ' ':
	score += 0.1918182;
      default:
	score += 0.0;
      }
  }
  return score;
}

char * xor_strings(char a[], char b[]) {
  int n = strlen(a);
  char * c = malloc(sizeof(char) * strlen(a));

  for (int i = 0; i < n; i++) {
    c[i] = a[i] ^ b[i];
  }

  return c;
}

int main(int argc, char *argv[])
{
  char in[128];

  FILE *fp;

  fp = fopen("4.txt", "r");

  while (fscanf(fp, "%s", in) != EOF) {
    int n = strlen(in);

    char *in_bytes = str2bytes(in);

    char b[n / 2];
    float max_xor_score = -1.0;
    char max_xor_string[n / 2];

    for (int i = 0; i < 0x100; i++) {
      for (int j = 0; j < n / 2; j++) {
	b[j] = i;
      }
      b[n / 2] = '\0';

      char *xor_string = xor_strings(in_bytes, b);
      float xor_score = score(xor_string, n / 2);

      if (max_xor_score < xor_score) {
	max_xor_score = xor_score;
	strcpy(max_xor_string, xor_string);
      }

      free(xor_string);
    }

    printf("%f|%s|", max_xor_score, in);
    for (int k = 0; k < n / 2; k++) {
      if (is_printable(max_xor_string[k]))
	printf("%c", max_xor_string[k]);
    }
    printf("\n");

  }

  return 0;
}
