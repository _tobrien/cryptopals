#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "base64.h"
#include "utils.h"

int main(int argc, char *argv[])
{
  char* s = argv[1];
  char* s_enc = b64_encode(str2bytes(s), strlen(s) / 2);
  printf("%s\n", s_enc);
  free(s_enc);
  return 0;
}
