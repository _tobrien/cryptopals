#include <stdio.h>
#include <stdlib.h>

void write_random_bytes(void *buf, int bytes)
{
  FILE *fp;
  fp = fopen("/dev/urandom", "r");
  fread(buf, 1, bytes, fp);
  fclose(fp);
}

unsigned short tob_rand(unsigned short n)
{
  unsigned short r;
  write_random_bytes(&r, sizeof(short));

  return r % n;
}
