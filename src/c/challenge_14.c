#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include "base64.h"
#include "tob_rand.h"
#include "block_lookup.h"

unsigned char B64_SUFFIX[] = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK";

void handle_errors(void)
{
  ERR_print_errors_fp(stderr);
  abort();
}

char *pkcs7 (int block_size, char plaintext[]) {
  int n = sizeof(char) * strlen(plaintext);
  int q = n / block_size;
  int r = n % block_size;
  int p = block_size - r;

  char *padded_plaintext = malloc(n + p);
  memcpy(padded_plaintext, plaintext, n);

  for (int i = n; i < n + p; i++) {
    padded_plaintext[i] = p;
  }

  padded_plaintext[n + p] = '\0';

  return padded_plaintext;
}

int ecb_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
		unsigned char *iv, unsigned char *ciphertext)
{
  EVP_CIPHER_CTX *ctx;
  int len;
  int ciphertext_len;

  /* Initialise context */
  if (!(ctx = EVP_CIPHER_CTX_new()))
    handle_errors();

  if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, iv))
    handle_errors();

  if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
    handle_errors();
  ciphertext_len = len;

  EVP_CIPHER_CTX_free(ctx);

  return ciphertext_len;
}

int ecb_decrypt(unsigned char *ciphertext, int ciphertext_len,
		unsigned char *key, unsigned char *iv, unsigned char *plaintext)
{
  EVP_CIPHER_CTX *ctx;
  int len;
  int plaintext_len;

  /* Initialise context */
  if (!(ctx = EVP_CIPHER_CTX_new()))
    handle_errors();

  if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, iv))
    handle_errors();

  if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
    handle_errors();
  plaintext_len = len;

  EVP_CIPHER_CTX_free(ctx);

  return plaintext_len;
}

char *encrypt(char* key, char *prefix, char *plaintext, char *suffix) {
  /* int suffix_len = (strlen(B64_SUFFIX) * 3) / 4; */
  /* memcpy(suffix, b64_decode(B64_SUFFIX), suffix_len) */
  /* suffix[suffix_len] = '\0'; */

  int i;
  int prefix_len = strlen(prefix);
  int plaintext_len = strlen(plaintext);
  int suffix_len = strlen(suffix);
  int padded_plaintext_len = prefix_len + plaintext_len + suffix_len;
    

  /* memcpy(suffix, b64_decode(B64_SUFFIX), suffix_len); */

  char padded_plaintext[padded_plaintext_len];
  char *ciphertext = malloc(sizeof(char) * padded_plaintext_len);

  /* Pad plaintext */
  for (i = 0; i < prefix_len; i++)
    padded_plaintext[i] = prefix[i];
  for (i = 0; i < plaintext_len; i++)
    padded_plaintext[prefix_len + i] = plaintext[i];
  for (i = 0; i < suffix_len; i++)
    padded_plaintext[prefix_len + plaintext_len + i] = suffix[i];

  ecb_encrypt(padded_plaintext, padded_plaintext_len, key, NULL, ciphertext);

  return ciphertext;
}

int main(int argc, char *argv[])
{
  /* Initialise the libarary */
  ERR_load_crypto_strings();
  OpenSSL_add_all_algorithms();
  OPENSSL_config(NULL);

  /* Set key and IV */
  unsigned char key[0x10];
  write_random_bytes(key, 0x10);

  int prefix_max_len = 0x80;
  int prefix_len = 10 + tob_rand(prefix_max_len - 10);
  char prefix[prefix_len];
  write_random_bytes(prefix, prefix_len);
  prefix[prefix_len] = '\0';

  int plaintext_max_len = 0x1000;
  int plaintext_len;
  unsigned char plaintext[plaintext_max_len];
  unsigned char *ciphertext;

  int suffix_len = (strlen(B64_SUFFIX) * 3) / 4;
  unsigned char *suffix;
  suffix = b64_decode(B64_SUFFIX);

  char *encrypted_suffix;
  encrypted_suffix = encrypt(key, prefix, "", suffix);

  unsigned char padding = 'A';
  int block_size = 0;

  /* Find block size
     Strategy: Generate increasingly longer padding plaintexts, hoping
     enough to fill two blocks with.  For each, try block sizes of
     increasing length until we find two matching blocks (start from
     block size of four to keep the chances of false positives small. */
  int candidate_block_size;
  for (int i = 1; i <= 0x80; i++) {
    if (0 < block_size)
      break;
    for (int j = 0; j < plaintext_max_len; j++) {
      if (j < i) {
	plaintext[j] = padding;
      } else {
	plaintext[j] = '\0';
      }
    }
    plaintext_len = i;
    ciphertext = encrypt(key, prefix, plaintext, suffix);

    char *block, *next_block;
    int offset;
    for (candidate_block_size = 4; candidate_block_size < 0x80; candidate_block_size++) {
      for (offset = 0; offset < prefix_len + plaintext_len; offset += candidate_block_size) {
	block = ciphertext + offset;
	next_block = ciphertext + offset + candidate_block_size;

	if (0 == memcmp(block, next_block, candidate_block_size)) {
	  block_size = candidate_block_size;
	  break;
	}
      }
    }

  }

  for (int i = 0; i < plaintext_max_len; i++)
    plaintext[i] = 0;

  int plaintext_padding_len = block_size - (prefix_len % block_size) - 1;
  plaintext_len = plaintext_padding_len + 1;
  for (int i = 0; i < plaintext_padding_len; i++)
    plaintext[i] = padding;

  int critical_block = prefix_len / block_size;
  char *ciphertext_block;
  for (int i = 0; i < 0x100; i++) {
    plaintext[plaintext_len - 1] = i;
    ciphertext = encrypt(key, prefix, plaintext, "");
    
    ciphertext_block = ciphertext + block_size * critical_block;

    put_block(block_size, ciphertext_block, i);

  }
  plaintext[plaintext_len - 1] = '\0';

  struct nlist *np;
  for (int i = 0; i < suffix_len; i++) {
    ciphertext = encrypt(key, prefix, plaintext, suffix + i);

    ciphertext_block = ciphertext + block_size * critical_block;
    np = get_block(block_size, ciphertext_block);
    printf("%c", np->c);
  }

  /* Clean up */
  EVP_cleanup();
  ERR_free_strings();

  return 0;
}
