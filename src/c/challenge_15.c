#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/conf.h>
#include <openssl/err.h>
#include "pkcs7.h"

void test_padding(int block_size, char plaintext[]) {
  if (pkcs7_check_padding(0x10, plaintext)) {
    printf("Valid!\n");
  } else {
    printf("Not valid!\n");
  }
}

int main(int argc, char *argv[])
{
  /* Initialise the libarary */
  /* ERR_load_crypto_strings(); */
  /* OpenSSL_add_all_algorithms(); */
  OPENSSL_config(NULL);

  /* Set key and IV */
  unsigned char key[] = "Hello";

  unsigned char *padding = pkcs7_pad(0x10, key);

  test_padding(0x10, padding);

  free(padding);

  unsigned char test[0x10];

  test[0] = 'I';
  test[1] = 'C';
  test[2] = 'E';
  test[3] = ' ';
  test[4] = 'I';
  test[5] = 'C';
  test[6] = 'E';
  test[7] = ' ';
  test[8] = 'B';
  test[9] = 'A';
  test[10] = 'B';
  test[11] = 'Y';
  test[12] = 4;
  test[13] = 4;
  test[14] = 4;
  test[15] = 4;
  test[16] = '\0';

  test_padding(0x10, test);

  test[12] = 5;
  test[13] = 5;
  test[14] = 5;
  test[15] = 5;

  test_padding(0x10, test);

  test[12] = 1;
  test[13] = 2;
  test[14] = 3;
  test[15] = 4;

  test_padding(0x10, test);

  /* Clean up */
  /* EVP_cleanup(); */
  ERR_free_strings();

  return 0;
}
