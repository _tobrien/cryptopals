#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include "xor.h"
#include "utils.h"

char * repeating_key_xor(char k[], char a[]) {
  int k_len = strlen(k);
  int a_len = strlen(a);
  char *out = malloc(sizeof(char) * a_len);

  for (int i = 0; i < a_len; i++) {
    out[i] = a[i] ^ k[i % k_len];
  }

  return out;
}

int main(int argc, char *argv[])
{
  char *k = argv[1];

  char a[128];

  while (fgets(a, sizeof(a), stdin) != 0) {
    a[strlen(a) - 1] = '\0';
    char * a_enc = repeating_key_xor(k, a);
    for (int i = 0; i < strlen(a); i++) {
      printf("%02x", a_enc[i]);
    }
    printf("\n");
    free(a_enc);
  }

  return 0;
}
