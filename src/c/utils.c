#include <string.h>
#include <stdlib.h>

char *str2bytes(char in[]) {
  int n = strlen(in) / 2;
  char *out = malloc(sizeof(char) * n);

  for (int i = 0; i < n; i++) {
    int b = 0;
    for (int j = 0; j < 2; j++) {
      char c = in[2 * i + j];
      if (c - '0' < 10) {
	b |= (c - '0') << 4 * (1 - j);
      }
      else if (c - 'a' < 6) {
	b |= (10 + c - 'a') << 4 * (1 - j);
      }
      else if (c - 'A' < 6) {
	b |= (10 + c - 'A') << 4 * (1 - j);
      }
    }
    out[i] = b;
  }

  return out;
}

int hamming_distance(char *buf_a, char *buf_b) {
  int d = 0;
  while (*buf_a != '\0') {
    int xor = *buf_a ^ *buf_b;
    while (xor != 0) {
      d += xor & 0x1;
      xor >>= 1;
    }
    
    buf_a++;
    buf_b++;
  }

  return d;
}
