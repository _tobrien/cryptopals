struct nlist {
  struct nlist *next;
  char *block;
  char c;
};

struct nlist *get_block(unsigned block_size, char *block);

struct nlist *put_block(unsigned block_size, char *block, char c);
