/* #include <stdio.h> */
#include <string.h>
#include <stdlib.h>

struct nlist {
  struct nlist *next;
  char *block;
  char c;
};

#define HASHSIZE 101
static struct nlist *hashtab[HASHSIZE];

unsigned hash(unsigned block_size, char *s) {
  unsigned hashval = 0;

  for (int i = 0; i < block_size; i++)
    hashval = *(s + i) + 31 * hashval;

  return hashval % HASHSIZE;
}

struct nlist *get_block(unsigned block_size, char *block) {
  struct nlist *np;

  for (np = hashtab[hash(block_size, block)]; np != NULL; np = np->next)
    if (memcmp(block, np->block, block_size) == 0)
      return np;

  return NULL;
}

struct nlist *put_block(unsigned block_size, char *block, char c) {
  struct nlist *np;
  unsigned hashval;

  if ((np = get_block(block_size, block)) == NULL) { /* not found */
    np = malloc(sizeof(*np));
    np->block = malloc(sizeof(char) * block_size);
    memcpy(np->block, block, block_size);
    np->c = c;
    if (np == NULL || np->block == NULL)
      return NULL;
    hashval = hash(block_size, block);
    np->next = hashtab[hashval];
    hashtab[hashval] = np;
  } else { /* already there */
    return NULL;
  }
  return np;
}
