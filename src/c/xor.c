#include <stdlib.h>
#include <ctype.h>


char * fixed_xor(char *a, char *b, int n) {
  char *out = malloc(sizeof(char) * n);
  for (int i = 0; i < n; i++) {
    out[i] = a[i] ^ b[i];
  }
  return out;
}

int e_score(char *in, int n) {
  int score = 0;
  for (int i = 0; i < n; i++) {
    char c = tolower(in[i]);
    if (c == 'e' || c == 't' || c == 'a' || c == 'o' || c == 'i' || c == 'n' ||
	c == ' ' || c == 's' || c == 'h' || c == 'r' || c == 'd' || c == 'l' ||
	c == 'u')
      score += 1;
  }

  return score;
}

float score(char * c, int n) {
  float score = 0.0;
  for (int i = 0; i < n; i++) {
    switch (tolower(c[i]))
      {
      case 'a':
	score += 0.0651738;
      case 'b':
	score += 0.0124248;
      case 'c':
	score += 0.0217339;
      case 'd':
	score += 0.0349835;
      case 'e':
	score += 0.1041442;
      case 'f':
	score += 0.0197881;
      case 'g':
	score += 0.0158610;
      case 'h':
	score += 0.0492888;
      case 'i':
	score += 0.0558094;
      case 'j':
	score += 0.0009033;
      case 'k':
	score += 0.0050529;
      case 'l':
	score += 0.0331490;
      case 'm':
	score += 0.0202124;
      case 'n':
	score += 0.0564513;
      case 'o':
	score += 0.0596302;
      case 'p':
	score += 0.0137645;
      case 'q':
	score += 0.0008606;
      case 'r':
	score += 0.0497563;
      case 's':
	score += 0.0515760;
      case 't':
	score += 0.0729357;
      case 'u':
	score += 0.0225134;
      case 'v':
	score += 0.0082903;
      case 'w':
	score += 0.0171272;
      case 'x':
	score += 0.0013692;
      case 'y':
	score += 0.0145984;
      case 'z':
	score += 0.0007836;
      case ' ':
	score += 0.1918182;
      default:
	score += 0.0;
      }
  }
  return score;
}
