#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include "base64.h"
#include "tob_rand.h"

unsigned char B64_SUFFIX[] = "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK";

void handle_errors(void)
{
  ERR_print_errors_fp(stderr);
  abort();
}

char *pkcs7 (int block_size, char plaintext[]) {
  int n = sizeof(char) * strlen(plaintext);
  int q = n / block_size;
  int r = n % block_size;
  int p = block_size - r;

  char *padded_plaintext = malloc(n + p);
  memcpy(padded_plaintext, plaintext, n);

  for (int i = n; i < n + p; i++) {
    padded_plaintext[i] = p;
  }

  padded_plaintext[n + p] = '\0';

  return padded_plaintext;
}

int ecb_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
		unsigned char *iv, unsigned char *ciphertext)
{
  EVP_CIPHER_CTX *ctx;
  int len;
  int ciphertext_len;

  /* Initialise context */
  if (!(ctx = EVP_CIPHER_CTX_new()))
    handle_errors();

  if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, iv))
    handle_errors();

  if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
    handle_errors();
  ciphertext_len = len;

  EVP_CIPHER_CTX_free(ctx);

  return ciphertext_len;
}

int ecb_decrypt(unsigned char *ciphertext, int ciphertext_len,
		unsigned char *key, unsigned char *iv, unsigned char *plaintext)
{
  EVP_CIPHER_CTX *ctx;
  int len;
  int plaintext_len;

  /* Initialise context */
  if (!(ctx = EVP_CIPHER_CTX_new()))
    handle_errors();

  if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, iv))
    handle_errors();

  if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
    handle_errors();
  plaintext_len = len;

  EVP_CIPHER_CTX_free(ctx);

  return plaintext_len;
}

void xor_buffer(unsigned char *buf, unsigned char *vals, int vals_len)
{
  for (int i = 0; i < vals_len; i++) {
    buf[i] = buf[i] ^ vals[i];
  }
}

int cbc_encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
		unsigned char *iv, unsigned char *ciphertext)
{
  int block_size = 0x10;
  int i = 0;
  int m = 0;
  char plaintext_block[block_size];
  char ciphertext_block[block_size];

  while (i < plaintext_len) {
    memcpy(plaintext_block, pkcs7(block_size, plaintext + i), block_size);
    xor_buffer(plaintext_block, iv, block_size);

    ecb_encrypt(plaintext_block, block_size, key, NULL, ciphertext_block);

    memcpy(ciphertext + i, ciphertext_block, block_size);
    memcpy(iv, ciphertext_block, block_size);

    i += block_size;
  }

  return i;
}

int cbc_decrypt(unsigned char *ciphertext, int ciphertext_len,
		unsigned char *key, unsigned char *iv,
		unsigned char *plaintext)
{
  int block_size = 0x10;
  int i = 0;
  int m = 0;
  char plaintext_block[block_size];
  char ciphertext_block[block_size];

  while (i < ciphertext_len) {
    memcpy(ciphertext_block, ciphertext + i, block_size);
    ecb_decrypt(ciphertext_block, block_size, key, NULL, plaintext_block);

    xor_buffer(plaintext_block, iv, block_size);

    memcpy(plaintext + i, plaintext_block, block_size);
    memcpy(iv, ciphertext_block, block_size);

    i += block_size;
  }

  return i;
}

unsigned short rand_padding_len(unsigned short min, unsigned short max)
{
  unsigned short padding_len = min + tob_rand(1 + max - min);

  return padding_len;
}

unsigned int count_repeated_blocks(unsigned char *buffer,
				   unsigned int buf_len,
				   unsigned int block_size)
{
  unsigned int repeated_blocks = 0;
  char seen_blocks[1 + buf_len / block_size][block_size];
  char test_block[block_size];
  char cur_block[block_size];

  unsigned int max_j = 0;
  unsigned int i;
  unsigned int j;
  int repeated;

  for (i = 0; i < buf_len; i += block_size) {
    repeated = 0;
    memcpy(cur_block, buffer + i, block_size);
    for (j = 0; j < max_j; j++) {
      memcpy(test_block, seen_blocks[j], block_size);
      if (memcmp(cur_block, test_block, block_size) == 0)
	repeated = 1;
    }
    if (repeated) {
      repeated_blocks += 1;
    } else {
      memcpy(seen_blocks[max_j], cur_block, block_size);
      max_j += 1;
    }
  }

  return repeated_blocks;
}

void encryption_oracle(char *plaintext, int plaintext_len)
{
  /* Generate a random key */
  unsigned char key[0x10];
  write_random_bytes(key, 0x10);

  /* Generate a random number in {0, 1} to detmine mode.  0 for ECB, 1
     for CBC. */
  unsigned short mode = tob_rand(2);

  /* Generate random prefix and suffix */
  unsigned short prefix_len, suffix_len;
  prefix_len = rand_padding_len(5, 10);
  suffix_len = rand_padding_len(5, 10);

  unsigned char prefix[prefix_len];
  unsigned char suffix[suffix_len];

  write_random_bytes(prefix, prefix_len);
  write_random_bytes(suffix, suffix_len);

  int padded_plaintext_len = prefix_len + plaintext_len + suffix_len;

  /* Pad plaintext with prefix and suffix */
  unsigned char padded_plaintext[padded_plaintext_len];
  memcpy(padded_plaintext, prefix, prefix_len);
  memcpy(padded_plaintext + prefix_len, plaintext, plaintext_len);
  memcpy(padded_plaintext + prefix_len + plaintext_len, suffix, suffix_len);

  unsigned char ciphertext[padded_plaintext_len];
  if (mode == 0) { /* If ECB */
    ecb_encrypt(padded_plaintext, padded_plaintext_len, key, NULL, ciphertext);
  } else { /* If CBC */
    char iv[0x80];
    write_random_bytes(iv, 0x80);
    cbc_encrypt(padded_plaintext, padded_plaintext_len, key, iv, ciphertext);
  }

  /* Detect ECB */
  unsigned int repeated_blocks = count_repeated_blocks(ciphertext, 
						       padded_plaintext_len,
						       0x10);

  /* if (repeated_blocks) { */
  /*   printf("ECB\n"); */
  /* } else { */
  /*   printf("CBC\n"); */
  /* } */
}

int main(int argc, char *argv[])
{
  /* Initialise the libarary */
  ERR_load_crypto_strings();
  OpenSSL_add_all_algorithms();
  OPENSSL_config(NULL);

  /* Set key and IV */
  unsigned char key[0x10];
  write_random_bytes(key, 0x10);
  /* unsigned char iv[0x80] = {0}; */

  int plaintext_max_len = 0x1000;
  unsigned char plaintext[plaintext_max_len];
  unsigned char ciphertext[plaintext_max_len];

  int suffix_len = (strlen(B64_SUFFIX) * 3) / 4;
  unsigned char suffix[suffix_len];
  memcpy(suffix, b64_decode(B64_SUFFIX), suffix_len);
  suffix[suffix_len] = '\0';

  char encrypted_suffix[suffix_len];
  ecb_encrypt(suffix, strlen(suffix), key, NULL, encrypted_suffix);

  unsigned char padding = 'A';
  int block_size = 0;

  /* Find block size */
  for (int i = 1; i <= 20; i++) {
    for (int j = 0; j < plaintext_max_len; j++) {
      ciphertext[j] = '\0';

      if (j < i) {
	plaintext[j] = padding;
      } else {
	plaintext[j] = '\0';
      }
    }
    memcpy(plaintext + i, suffix, suffix_len);
    ecb_encrypt(plaintext, strlen(plaintext), key, NULL, ciphertext);

    /* TODO: Why suffix_len - 10 ?? */
    int is_block_size = 0 == memcmp(ciphertext + i, encrypted_suffix, suffix_len - 10);
    if (is_block_size) {
      block_size = i;
      break;
    }

  }

  unsigned char test_ciphertext_block[block_size];
  unsigned char cur_ciphertext_block[block_size];

  for (int wow = 0; wow < suffix_len; wow++) {
    for (int j = 0; j < plaintext_max_len; j++) {
      ciphertext[j] = '\0';

      if (j < block_size - 1) {
	plaintext[j] = padding;
      } else {
	plaintext[j] = '\0';
      }
    }
    memcpy(plaintext + block_size - 1, suffix + wow, suffix_len - wow);
    ecb_encrypt(plaintext, strlen(plaintext), key, NULL, ciphertext);
    memcpy(test_ciphertext_block, ciphertext, block_size);
    
    for (int i = 0; i < 0x100; i++) {
      for (int j = 0; j < plaintext_max_len; j++) {
	ciphertext[j] = '\0';

	if (j < block_size - 1) {
	  plaintext[j] = padding;
	} else {
	  plaintext[j] = '\0';
	}
      }
      plaintext[block_size - 1] = i;

      memcpy(plaintext + block_size, suffix + wow, suffix_len - wow);

      ecb_encrypt(plaintext, strlen(plaintext), key, NULL, ciphertext);
    
      memcpy(cur_ciphertext_block, ciphertext, block_size);

      if (!memcmp(test_ciphertext_block, cur_ciphertext_block, block_size))
	printf("%c", i);
    }
  }
  printf("\n");

  /* Clean up */
  EVP_cleanup();
  ERR_free_strings();

  return 0;
}
