#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int BLOCK_SIZE_LEN = 8;

int num_repeated_blocks(char cipher[])
{
  int repeated_blocks = 0;
  int previously_seen;
  char seen_blocks[strlen(cipher) / (BLOCK_SIZE_LEN)][BLOCK_SIZE_LEN];
  int max_seen_blocks = 0;
  char seen_block[BLOCK_SIZE_LEN];
  char current_block[BLOCK_SIZE_LEN];

  for (int i = 0; i < strlen(cipher); i += BLOCK_SIZE_LEN) {
    char* block_pointer = cipher + i;
    strncpy(current_block, block_pointer, BLOCK_SIZE_LEN);

    previously_seen = 0;
    for (int j = 0; j < max_seen_blocks; j++) {
      strncpy(seen_block, seen_blocks[j], BLOCK_SIZE_LEN);
      if (strncmp(seen_block, current_block, BLOCK_SIZE_LEN) == 0) {
	repeated_blocks += 1;
	previously_seen = 1;
      }
    }

    if (previously_seen == 0)
      strncpy(seen_blocks[max_seen_blocks++], current_block, BLOCK_SIZE_LEN);

  }

  return repeated_blocks;
}

int main(int argc, char *argv[])
{
  char cipher[320];
  char encrypted_cipher[320];
  int repeated_blocks;
  int max_repeated_blocks = 0;

  FILE *fp;
  char *line;
  size_t len = 0;
  ssize_t read;

  fp = fopen("8.txt", "r");
  while ((read = getline(&line, &len, fp)) != -1) {
    strncpy(cipher, line, 320);
    cipher[320] = '\0';
    repeated_blocks = num_repeated_blocks(cipher);
    if (max_repeated_blocks < repeated_blocks) {
      strncpy(encrypted_cipher, cipher, 320);
      max_repeated_blocks = repeated_blocks;
    }
  }

  printf("%d\t%s\n", max_repeated_blocks, encrypted_cipher);

  return 0;
}
