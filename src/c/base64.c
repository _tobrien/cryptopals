#include <stdlib.h>
#include <string.h>

const char codes[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

int b64_index(char c) {
  int index = 0;
  if (0 <= c - 'A' && c - 'A' <= 'Z' - 'A')
    index = c - 'A';
  if (0 <= c - 'a' && c - 'a' <= 'z' - 'a')
    index = 26 + c - 'a';
  if (0 <= c - '0' && c - '0' <= '9' - '0')
    index = 52 + c - '0';
  if (c == '+')
    index = 62;
  if (c == '/')
    index = 63;

  return index;
}

char * b64_encode(char* bytes, int len)
{
  int n = (len * 4) / 3;
  char *out = malloc(sizeof(char) * n);
  int b;

  for (int i = 0; i < len; i += 3) {
    b = (bytes[i] & 0xFC) >> 2;
    out[4 * (i/3)] = codes[b];

    b = (bytes[i] & 0x3) << 4;
    if (i + 1 < len) {
      b |= (bytes[i+1] & 0xF0) >> 4;
      out[1 + 4 * (i/3)] = codes[b];

      b = (bytes[i+1] & 0xF) << 2;
      if (i + 2 < len) {
	b |= (bytes[i+2] & 0xC0) >> 6;
	out[2 + 4 * (i/3)] = codes[b];

	b = bytes[i+2] & 0x3F;
	out[3 + 4 * (i/3)] = codes[b];
      }
      else {
	out[2 + 4 * (i/3)] = codes[b];
	out[3 + 4 * (i/3)] = codes[64];
      }
    }
    else {
      out[1 + 4 * (i/3)] = codes[b];
      out[2 + 4 * (i/3)] = codes[64];
      out[3 + 4 * (i/3)] = codes[64];
    }
  }

  return out;
}

char * b64_decode(char in[]) {

  int n = (strlen(in) * 3) / 4;

  char *out = malloc(sizeof(char) * n);
  
  int index;
  char c;
  int j;
  for (int i = 0; i < strlen(in); i += 4) {
    j = 3 * (i / 4);
    c = in[i];
    index = b64_index(c);

    out[j] = index << 2;

    c = in[i + 1];

    index = b64_index(c);

    out[j] |= index >> 4;

    out[j + 1] = (index & 0xF) << 4;

    c = in[i + 2];
    index = b64_index(c);

    out[j + 1] |= index >> 2;

    out[j + 2] = (index & 0x3) << 6;

    c = in[i + 3];
    index = b64_index(c);
    
    out[j + 2] |= index;
  }

  /* out[n] = '\0'; */

  return out;
  
}
