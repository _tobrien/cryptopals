#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *pkcs7_pad (int block_size, char plaintext[]) {
  int n = sizeof(char) * strlen(plaintext);
  int r = n % block_size;
  if (r == 0)
    r = block_size;
  int p = block_size - r;

  char *padded_plaintext = malloc(n + p);
  memcpy(padded_plaintext, plaintext, n);

  for (int i = n; i < n + p; i++)
    padded_plaintext[i] = p;

  padded_plaintext[n + p] = '\0';

  return padded_plaintext;
}

int pkcs7_check_padding (int block_size, char plaintext[]) {
  int n = strlen(plaintext);
  char p = plaintext[block_size * (1 + (n - 1) / block_size) - 1];
  
  if (block_size <= p)
    return 1;

  for (int i = n - 1; n - p <= i; i--)
    if (plaintext[i] != p)
      return 0;

  return 1;
}
