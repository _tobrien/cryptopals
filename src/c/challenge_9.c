#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *pkcs7 (int block_size, char plaintext[]) {
  int n = sizeof(char) * strlen(plaintext);
  int q = n / block_size;
  int r = n % block_size;
  int p = block_size - r;

  char *padded_plaintext = malloc(n + p);
  memcpy(padded_plaintext, plaintext, n);

  for (int i = n; i < n + p; i++) {
    padded_plaintext[i] = p;
  }

  padded_plaintext[n + p] = '\0';

  return padded_plaintext;
}

int main(int argc, char *argv[])
{
  char test_plaintext[] = "YELLOW SUBMARINE";
  char *padded_plaintext = pkcs7(20, test_plaintext);
  free(padded_plaintext);
      
  return 0;
}

