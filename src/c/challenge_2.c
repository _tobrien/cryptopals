#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "xor.h"
#include "utils.h"

int main(int argc, char *argv[])
{
  char *a = argv[1];
  char *a_enc = str2bytes(a);
  char *b = argv[2];
  char *b_enc = str2bytes(b);

  char *c = fixed_xor(a_enc, b_enc, strlen(a) / 2);

  for (int i = 0; i < strlen(a) / 2; i++) {
    printf("%x", c[i]);
  }
  printf("\n");

  free(a_enc);
  free(b_enc);
  free(c);

  return 0;
}
